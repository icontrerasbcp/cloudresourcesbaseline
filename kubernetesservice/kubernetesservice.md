| Name | Description          |
| ------------- | ----------- |
| Autor      | Ivan Contreras |
| Area TI     | Arquitectura Cloud |
| Nube     | Microsoft Azure     |
| Categoría     | Integration     |
| Recurso     | API Management     |
| Modelo despliegue     | PaaS     |
| Código     | API     |


# API Management ![alt text](https://azure.microsoft.com/svghandler/api-management?width=80&height=50 "AM")

## ¿Qué es?  

API Manager es un componente de integración en la nube Microsoft Azure que permite exponer y administrar, de manera segura, APIs de nube u on premise.

**Lineamientos de seguridad BCP implementados:**

* API Key
* Mutual Authentication
* Auditoría
* CORS

## ¿Para qué lo puedo usar?(casos de uso)

El principal uso de API Management es la exposición, gestión y protección de APIs (en nube) de negocio y/o experiencia. 

También, se puede usar como punto de entrada a endpoints on premise (en BCP Chorrillos y La Molina) utilizando la integración con Azure Virtual Network y el canal MPLS (Express Route) del banco.

Para mayor información sobre las funcionalidades del componente puede visitar el siguiente [enlace](https://docs.microsoft.com/en-us/azure/api-management/api-management-key-concepts).

**Nota**

> Solo los API Manager Developer y Premium tienen integración con Azure Virtual Network. Puede consultar las capacidades por tier en el siguiente [enlace](https://azure.microsoft.com/en-us/pricing/details/api-management/).

## ¿Cómo esta permitido usarlo? (lineamientos de uso)

### Variaciones Permitidas

Variaciones a nivel de redes:

* API Management integrado a Azure Virtual Network.
* API Management sin integración a Azure Virtual Network.

De la misma manera, pueden haber variaciones a nivel de la accesibilidad que se le quieren dar a las API que API Management gestiona. 

* Pública - Sin conectividad directa a Azure Virtual Network.
* Interna - Con conectividad directa a Azure Virtual Network con una IP Pública y acceso a recursos internos. 
* Privada - Con conectividad directa a Azure Virtual Netowrk con una IP Privada y acceso a recursos internos. 

### Regiones Permitidas

* East US 2

### Dependencias

* Ninguna

### Restricciones de uso

* Ninguno 

## ¿Cómo lo aprovisiono?

### Escenario 1: Sin integración a Azure Virtual Network

 Endpoint de aplicación en nube expuesto de manera pública.

*  Automatizado: Copiar los archivos apim-scenario1.json y apim-scenario1-params.json en la misma carpeta y con la herramienca Azure CLI ejecutar:
    
```
az group deployment create --resource-group $RG --template-file apim-scenario1.json --parameters apim-scenario1-params.json
```

Nota
> Este comando también se puede ejecutar desde un job de Jenkins, pipeline de Bitbucket o job de Bamboo. 

*  Manual: Se debe ingresar los parametros respetando la nomenclatura indicado en el grupo de recurso adecuado.
    [Aprovisionalo aqui](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fbitbucket.org%2Ficontrerasbcp%2Fcloudresourcesbaseline%2Fraw%2F271d15d9e566f39d62942ede08327c9b7337f71a%2Fstarterapimdeploy.json)

## Consideraciones

### Mutual Authentication

API Manager actúa como proxy en el proceso de Mutual Authentication. No requiere de ninguna configuración especial dado que por   defecto permite el tránsito del certificado hasta el backend. Considerar que:

* El SSL Termination deberá ocurrir en el endpoint del backend. 
* En el API Manager no se requiere cargar ningún certificado.
* Mutual AUthentication debe ser usado en el caso de que no se pueda contar con un API Manager con integración a Azure Virtual Network y el endpoint de los servicios de la aplicación es expuesto de manera pública.


## Contribuidores

| Nombre | Email          |  Area  |
| ------------- | ----------- | -------| 
| Carlos Zela      | [carloszela@bcp.com.pe](mailto:carloszela@bcp.com.pe) | ATI -TEC  |
| Nicanor Sachahuaman   | [nsachahuaman@bcp.com.pe](mailto:nsachahuaman@bcp.com.pe)    | ATI - TEC|
| Gerardo Guzmán   | [gguzman@bcp.com.pe](mailto:gguzman@bcp.com.pe)    | ATI - SEG|


## Costo

Puede consultar el [siguiente](https://azure.microsoft.com/en-us/pricing/details/api-management/) enlace para tener un aproximado del costo del componente 