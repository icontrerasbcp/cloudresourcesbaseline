| Name | Description          |
| ------------- | ----------- |
| Autor      | Ivan Contreras |
| Area TI     | Arquitectura Cloud |
| Nube     | Microsoft Azure     |
| Categoría     | Containers     |
| Recurso     | Container Registry     |
| Modelo despliegue     | PaaS     |
| Código     | ACR     |


# Container Registry ![alt text](https://azure.microsoft.com/svghandler/container-registry/?width=80&height=50 "ACR")

## ¿Qué es?  

Container Registry es un componente que permite almacenar imágenes de contenedores de manera privada para el banco. 

**Lineamientos de seguridad BCP implementados:**

* Headless Authentication (sin intereacción)

## ¿Para qué lo puedo usar?(casos de uso)

Container Registry es utilizado para almacenar imágenes de contenedores de una aplicación. En el banco se tiene un Container Registry por ambiente y por aplicación.

Para mayor información sobre las funcionalidades del componente puede visitar el siguiente [enlace](https://docs.microsoft.com/en-us/azure/container-registry/container-registry-intro).

## ¿Cómo esta permitido usarlo? (lineamientos de uso)

### Variaciones Permitidas

* No presenta variaciones dado que sólo tiene un modo de despliegue.

### Regiones Permitidas

* East US 2.

### Dependencias

* Ninguna.

### Restricciones de uso

* Ninguno.

## ¿Cómo lo aprovisiono?

Copiar los archivos acr-scenario1.json y acr-scenario1-params.json en la misma carpeta y con la herramienca Azure CLI ejecutar:
    
```
az group deployment create --resource-group $RG --template-file acr-scenario1.json --parameters acr-scenario1-params.json
```

Nota
> Este comando también se puede ejecutar desde un job de Jenkins, pipeline de Bitbucket o job de Bamboo. 

## Consideraciones

* Para poder realizar el despliegue sin interacción de usuarios requiere de un service principal. Para mayor detalle puede visitar el siguiente [enlace](https://docs.microsoft.com/en-us/azure/container-registry/container-registry-auth-service-principal).

## Contribuidores

| Nombre | Email          |  Area  |
| ------------- | ----------- | -------| 


## Costo

Puede consultar el [siguiente](https://azure.microsoft.com/en-us/pricing/details/container-registry/) enlace para tener un aproximado del costo del componente 